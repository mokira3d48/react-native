import API_TOKEN from './api_token';

export function getMovieFromAPI(txt, page) {
    const url = 'https://api.themoviedb.org/3/search/movie?api_key=' + API_TOKEN + '&language=fr&query=' + txt + '&page=' + page;
    return fetch(url)
        .then((response) => response.json())
        //.catch((error)   => console.log(error));

}

export function getMovieDetailFromAPI(id) {
    const url = 'https://api.themoviedb.org/3/movie/' + id + '?api_key=' + API_TOKEN + '&language=fr';

    return fetch(url)
        .then((response) => response.json())
        //.catch((error)   => console.log(error));
}



