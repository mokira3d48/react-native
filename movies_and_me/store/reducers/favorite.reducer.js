// on cree un state initial :
const INITIAL_STATE = {favoritesMovie : []};

/**
 * Reducer de gestion des films favorites.
 */
function toggleFavorite(state = INITIAL_STATE, action) {
    let _next_state;

    switch(action.type) {
        case 'TOGGLE_FAVORITE' :
            const _FAVORITE_MOVIE_INDEX = state.favoritesMovie.findIndex(item => item.id === action.value.id);

            // si le film existe deja dans les favorites :
            if (_FAVORITE_MOVIE_INDEX !== -1) {
                // on supprime le film des favorite :
                _next_state = {
                    // on remet toujour l'enciens [state] :
                    ...state,
                    
                    // on filtre la liste des films favorites en enlevant le film en question :
                    favoritesMovie : state.favoritesMovie.filter((item, index) => index !== _FAVORITE_MOVIE_INDEX),
                };
            }
            else { // sinon si le film n'existe dans les favorites :
                // on ajoute le film dans les favorites :
                _next_state = {
                    // on remet toujour l'enciens [state] :
                    ...state,
                    
                    // on reprendre la liste des enciennes favorites et on ajoute le nouveau :
                    favoritesMovie : [action.value, ...state.favoritesMovie],
                };
            }

            return _next_state || state;

        default : return state;
    }

}

// on exporte le reducer :
export default toggleFavorite;

