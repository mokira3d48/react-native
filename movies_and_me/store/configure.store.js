import {createStore}    from 'redux';
import toggleFavorite   from './reducers/favorite.reducer';


/**
 * creation et exportation du store pour le reducer [favorite]
 * ...
 */
export default createStore(toggleFavorite);

