import {createStackNavigator, createAppContainer} from 'react-navigation';
import Search from '../components/search';

const SSNavigator = createStackNavigator({
    Search : {
        screen : Search,
        navigationOption : {
            title : "Rechercher"
        }
    }
});


export default createAppContainer(SSNavigator);
