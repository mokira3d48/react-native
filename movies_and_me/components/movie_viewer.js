/** Importation globale. */
import React                from 'react';
import {ScrollView}         from 'react-native';
import {View}               from 'react-native';
import {Text}               from 'react-native';
import {Image}              from 'react-native';
import {TouchableOpacity}   from 'react-native';
import {Share}              from 'react-native';
import {Platform}           from 'react-native';
import {connect}            from 'react-redux';
import numeral              from 'numeral';

/** Importation des styles. */
import STYLE    from './styles/movie_viewer.style';

/** Importation des composants. */
import Loader  from './loader';

/** Autres. */
import {getMovieDetailFromAPI} from '../API/tmdb_api';
import FilmItem from './film_item';


const MAP_STATE_TO_PROPS = (state) => {
    //console.log(state);
    return {
        favoritesMovie : state.favoritesMovie
    };

};


class MovieViewer extends React.Component {

    /*static navigationOption = ({navigation}) => {
        if (navigation.state.params != undefined && Platform.OS === 'ios') {
            return {
                headerRight : (
                    <TouchableOpacity
                        style   = {STYLE.shareTouchableHRight}
                        onPress = {() => navigation.state.params.shareMovieF()}
                        >
                            <Image
                                style  = {STYLE.shareImage}
                                source = {require('../assets/favicon.png')}
                                />
    
                        </TouchableOpacity>
                ),
            }
        }
    }*/

    constructor(props) {
        super(props);

        this.imguri  = 'https://image.tmdb.org/t/p/w300';
        this.movieid = this.props.route.params.movieid;
        this.state   = {
            movieDetail : undefined,
            isLoading   : true,
            isFavorite  : false,
        }

        this._shareMovie = this._shareMovie.bind(this);

    }

    componentDidMount() {
        getMovieDetailFromAPI(this.movieid)
            .then(data => {
                this.setState({
                    movieDetail : data,
                    isLoading   : false,
                    isFavorite  : this.props.favoritesMovie.findIndex(item => item.id === data.id) !== -1,
                }, () => {
                    this._updateNavigationParams();

                });
            });

    }

    componentDidUpdate() {
        //this.setState( {
          //  ...this.state,

        //});
        //console.log(this.props.favoritesMovie);
    }

    _customCompare(str1, str2) {
        let _str1 = '';
        let _str2 = '';

        for (let i = 0; i < str1.length; i ++ ) _str1 += (str1[i] != ' ')? str1[i].toLowerCase() : '';
        for (let i = 0; i < str2.length; i ++ ) _str2 += (str2[i] != ' ')? str2[i].toLowerCase() : '';

        //console.log(_str2);

        if (_str1.length == _str2.length) {
            for (let i = 0; i < _str1.length; i++)
                if (_str1[i] != _str2[i])
                    return false;
            
            return true;
        }
        
        return false;

    }

    _updateNavigationParams() {
        /*this.props.navigation.setParams({
            shareMovieF : this._shareMovie,
            movie : this.state.movie,
        });*/

    }

    _showFavorite() {
        if (this.state.isFavorite)
            return require('../assets/ic_favorite.png');
        
        else
            return require('../assets/ic_not_favorite.png');

    }

    _shareMovie() {
        Share.share({
            title   : this.state.movieDetail.title,
            message : this.state.movieDetail.overview,
        });
    }

    _displayFloatingActionButton() {
       /* const _MOVIE = this.state.movieDetail;

        if (_MOVIE != undefined && Platform.OS === 'android')
            return (
                <TouchableOpacity
                    style   = {STYLE.shareTouchableFloating}
                    onPress = {() => this._shareMovie()}
                    >

                        <Image
                            style  = {STYLE.shareImage}
                            source = {require('../assets/ic_share.png')} 
                            />
                
                </TouchableOpacity>
            );
        */
    }

    _showMovieDetail() {
        //if (this.state.movieDetail != undefined)
            //console.log(this.state.movieDetail.title, this.state.isFavorite);
        //console.log(this.state.novieDetail);
        if (this.state.movieDetail != undefined)
            return (
                <ScrollView style = {STYLE.detailContainer}>
                    <Image style = {STYLE.image} source={{uri : this.imguri + this.state.movieDetail.backdrop_path}}></Image>
                    <TouchableOpacity
                        style   = {STYLE.favoriteStyle} 
                        onPress = {() => this._toggleFavorite()}
                        >
                            <Image
                                style  = {STYLE.favoriteImage}
                                source = {this._showFavorite()} 
                                />
                    </TouchableOpacity>
                    <Text style = {STYLE.originalTitle}>{
                        this.state.movieDetail.original_title
                    }</Text>
                    <Text style = {STYLE.title}>{
                        //this.state.movieDetail.title != this.state.movieDetail.original_title ? this.state.movieDetail.title : null
                        !this._customCompare(this.state.movieDetail.title, this.state.movieDetail.original_title) ? this.state.movieDetail.title : null
                    }</Text>
                    <Text style = {STYLE.tagline}>{this.state.movieDetail.tagline}</Text>
                    <Text style = {STYLE.description}>{this.state.movieDetail.overview}</Text>

                    <View style = {STYLE.infoContainer}>
                        <View style = {STYLE.infoRow}>
                            <Text style = {STYLE.attrName}>Sortie le : </Text>
                            <Text style = {STYLE.dataValue}>{"" + this.state.movieDetail.release_date}</Text> 
                        </View>
                        <View style = {STYLE.infoRow}>
                            <Text style = {STYLE.attrName}>Note /10 : </Text>
                            <Text style = {STYLE.dataValue}>{"" + this.state.movieDetail.vote_average}</Text> 
                        </View>
                        <View style = {STYLE.infoRow}>
                            <Text style = {STYLE.attrName}>Nombre de vote : </Text>
                            <Text style = {STYLE.dataValue}>{"" + this.state.movieDetail.vote_count}</Text> 
                        </View>
                        <View style = {STYLE.infoRow}>
                            <Text style = {STYLE.attrName}>Popularité : </Text>
                            <Text style = {STYLE.dataValue}>{"" + this.state.movieDetail.popularity}</Text> 
                        </View>
                        <View style = {STYLE.infoRow}>
                            <Text style = {STYLE.attrName}>Budget ($US) : </Text>
                            <Text style = {STYLE.dataValue}>{"" + numeral(this.state.movieDetail.budget).format('0,0[.]00')}</Text> 
                        </View>
                        <View style = {STYLE.infoRow}>
                            <Text style = {STYLE.attrName}>Genre(s) : </Text>
                            <Text style = {STYLE.dataValue}>{
                                "" + this.state.movieDetail.genres.map(function(genre) {
                                    return genre.name;
                                }).join(' / ')
                            }
                            </Text> 
                        </View>
                        <View style = {STYLE.infoRow}>
                            <Text style = {STYLE.attrName}>Langue d'origine : </Text>
                            <Text style = {STYLE.dataValue}>{"" + this.state.movieDetail.original_language}</Text> 
                        </View>
                        <View style = {STYLE.infoRow}>
                            <Text style = {STYLE.attrName}>Compagnie(s) : </Text>
                            <Text style = {STYLE.dataValue}>{
                                "" + this.state.movieDetail.production_companies.map(function(comp) {
                                    return comp.name;
                                }).join(' / ')
                            }
                            </Text> 
                        </View>
                        <View style = {STYLE.infoRow}>
                            <Text style = {STYLE.attrName}>Pays(s) : </Text>
                            <Text style = {STYLE.dataValue}>{
                                "" + this.state.movieDetail.production_countries.map(function(country) {
                                    return country.name;
                                }).join(' / ')
                            }
                            </Text> 
                        </View>
                    </View>
                </ScrollView>
            );
    }

    _showLoader() {
        if (this.state.isLoading)
            return (
                <Loader />
            );

        return null;

    }

    _toggleFavorite() {
        const _ACTION = {
            type  : "TOGGLE_FAVORITE",
            value : this.state.movieDetail,
        };
        
        // on envoye une action au reducer :
        this.props.dispatch(_ACTION);

        this.setState({
            ...this.state,
            isFavorite : !this.state.isFavorite,
        });

    }

    render() {
        //console.log(this.props);
        return (
            <View style = {STYLE.container}>
                {this._showMovieDetail()}
                {this._showLoader()}
                {this._displayFloatingActionButton()}
            </View>
        );
    }
}

export default connect(MAP_STATE_TO_PROPS, null)(MovieViewer);
