import React from 'react'
import {StyleSheet, View, Text, Image} from 'react-native'

//import img from '../assets/favicon.png';

const IMAGE_SRC = '../assets/loader_img.gif';

class Loader extends React.Component {

    render() {
        const top = this.props.top ? Number(this.props.top) : 0;
        return (
            <View style = {[style.container, {top : top}]}>
                <Image style = {style.image} source = {require(IMAGE_SRC)}></Image>
            </View>
        );

    }

}


const style = StyleSheet.create({
    container : {
        position : 'absolute',
        right : 0,
        top : 60,
        bottom : 0,
        left : 0,
        alignItems : 'center',
        justifyContent : 'center',
    },
    image : {
        flex : 1,
        opacity : 1,
    },
});

export default Loader;

