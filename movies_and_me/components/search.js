import React from 'react'
import { StyleSheet, View, Button, TextInput, FlatList, Platform} from 'react-native';

import {connect} from 'react-redux';

/** Importation des styles. */
import APP      from './styles/app.style';
import STYLE    from './styles/search.style';


// import films            from '../helpers/filmsdata';
import FilmItem          from './film_item';
import Loader            from './loader';
import {getMovieFromAPI} from '../API/tmdb_api';

// CONSTANTE :
const DEFAULT_SEARCH_TEXT = (new Date().getFullYear() - 1).toString();

const MAP_STATE_TO_PROPS = (state) => {
    return {
        favoritesMovie : state.favoritesMovie,
    };

}

class Search extends React.Component {
    
    constructor(props) {
        super(props);

        this.state = {
            movieData : [],
            isLoading : false,
        };

        this.text = DEFAULT_SEARCH_TEXT;
        this.page = 0;
        this.total_page = 0;

    }

    componentDidMount() {
         // On charge les films nouvellement sortis :
         this._loadFilm();

    }

    _makeid(length) {
        let result           = '';
        let characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        let charactersLength = characters.length;
        
        for ( var i = 0; i < length; i++ )
           result += characters.charAt(Math.floor(Math.random() * charactersLength));

        return result;

     }

    _loadFilm() {
        if (!this.state.isLoading)
            this.setState({ isLoading : true}, () => {
                getMovieFromAPI(this.text, this.page + 1)
                    .then(data => {
                        this.total_page = data.total_pages;
                        this.page       = data.page;
                        //console.log(data);
                        this.setState({
                            movieData : this.state.movieData.concat(data.results),
                            isLoading : false,
                        });
                    });
            });

    }

    _setText(txt) {
        this.text = txt.trim();
        //this.page = 0;
        //this.total_page = 0;
    }

    _searchMovie() {
        if (this.text != "") {
            this.page = 0;
            this.total_page = 0;
            this.setState({
                movieData : [],
            },
                () => this._loadFilm() // cette fonction est appelée lorsque setState finit son execution.
            );
        }

    }

    _showLoader() {
        if (this.state.isLoading)
            return (
                <Loader top='60' />
            );
        
        return null;

    }

    _showMovie = (movieid) => {
        //console.log(movie);
        this.props.navigation.navigate('Description du film', {movieid : movieid});

    }

    render() {
        /*return (
            <View style={style.container}>
                <View style = { style.search_bar}>
                    <TextInput 
                        onChangeText    = {(text) => this._setText(text)}
                        onSubmitEditing = {() => this._searchMovie()}
                        style           = {style.textinput} placeholder="Titre du film"
                    />
                    <Button 
                        style   = {style.button} 
                        onPress = {() => this._searchMovie()} 
                        title   = "Rechercher" 
                    />
                </View>
                <View style = { style.list_container}>
                    <FlatList 
                        style        = {style.list_style}
                        data         = {this.state.movieData} 
                        keyExtractor = {(item) => item.id.toString()} 
                        renderItem   = {({item}) => <FilmItem film={item}/>}
                        onEndReachedThreshold = {5}
                        onEndReached = {() => {                        
                            //console.log("onEndReached");
                            //console.log(this.page, this.total_page);

                            if (this.page < this.total_page)
                                this._loadFilm();

                        }}
                    />
                </View>

                {this._showLoader()}

            </View>
        );*/

        return (
            <View style={STYLE.container}>
                <View style = { STYLE.search_bar}>
                    <TextInput 
                        onChangeText         = {(text) => this._setText(text)}
                        onSubmitEditing      = {() => this._searchMovie()}
                        style                = {STYLE.textinput} placeholder="Titre du film"
                        placeholderTextColor = {APP.COLOR.INPUT_TEXT_PLACEHOLDER}
                    />
                </View>
                <View style = { STYLE.list_container}>
                    <FlatList 
                        style        = {STYLE.list_style}
                        data         = {this.state.movieData} 
                        extraData    = {this.props.favoritesMovie}
                        keyExtractor = {(item) => item.id.toString() + this._makeid(8)} 
                        renderItem   = {({item}) => <FilmItem 
                                                        film={item} 
                                                        isFavorite  = {(this.props.favoritesMovie.findIndex(film => film.id === item.id) !== -1) ? true : false} 
                                                        showDetails = {this._showMovie}
                                                    />}
                        onEndReachedThreshold = {0.5}
                        onEndReached = {() => {                        
                            if (this.page < this.total_page)
                                this._loadFilm();

                        }}
                    />
                </View>

                {this._showLoader()}

            </View>
        );
    }
}


export default connect(MAP_STATE_TO_PROPS, null)(Search);

