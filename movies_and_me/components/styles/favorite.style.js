import APP          from './app.style';
import {StyleSheet} from 'react-native';

const STYLE = StyleSheet.create({
    container : {
        backgroundColor : APP.COLOR.BACKGROUD_COLOR,
        marginTop : 0,
        flex : 1,
        paddingBottom : 10,
    },
    ////////////////////////////////////////////////////////////
    list_container : {
        flex : 15,
        backgroundColor : APP.COLOR.BACKGROUD_COLOR,
        marginTop : 10,
    },
    list_style : {
        flex : 1,
    },
    loading_container : {
        position : 'absolute',
        left : 0,
        right : 0,
        top : 100,
        bottom : 0,
        alignItems : 'center',
        justifyContent : 'center',
    },
});


export default STYLE;
