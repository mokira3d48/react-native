
const APP = {
    COLOR : {
        BACKGROUD_COLOR         : '#060621',
        INPUT_TEXT_BACKCOLOR    : '#181f2f',
        INPUT_TEXT_PLACEHOLDER  : '#72727c',
        INPUT_TEXT_BORDER_COLOR : '#5d6297',
        TOOL_BAR_COLOR          : '#2f1f43',
        IMAGE_BACK_COLOR        : '#181f2f99',
        TEXT_COLOR              : '#ccc',
        //MENU_BAR_COLOR          : '#10192c',
        MENU_BAR_COLOR          : '#2f1f43de',
        ASSETS                  : '#9966a9',
        FAVORITE                : '#ea80d0'
    },
};

export default APP;
