import APP from './app.style';
import {StyleSheet} from 'react-native';


const STYLE = StyleSheet.create({
    container : {
        backgroundColor : APP.COLOR.BACKGROUD_COLOR,
        //marginTop : 40,
        flex : 1,
        paddingBottom : 10,
    },
    search_bar : {
        marginTop : 1,
        padding : 5,
        flexDirection : 'row',
        backgroundColor : APP.COLOR.TOOL_BAR_COLOR,
    },
    textinput : {
        flex : 1,
        height : 50,
        marginLeft : 1,
        marginRight : 1,
        padding : 15,
        fontSize : 18,
        //backgroundColor : APP.COLOR.INPUT_TEXT_BACKCOLOR,
        backgroundColor : APP.COLOR.BACKGROUD_COLOR,
        color : APP.COLOR.TEXT_COLOR,
        borderColor : APP.COLOR.INPUT_TEXT_BORDER_COLOR,
        borderWidth : 1,
        //borderRadius : 10,
        shadowColor: "#00ffee",
        shadowOffset: {
	        width: 0,
	        height: 3,
        },
        shadowOpacity: 0.5,
        shadowRadius: 11.14,

        //borderWidth : 1,
        //borderColor : '#000',
        ...Platform.select({
            ios : {
                borderRadius : 200,
            },
            android : {
                borderRadius : 10,
            }
        }),
    },
    /*button : {
        flex : 1,
        height : 50,
        marginLeft : 1,
        marginRight : 1,
        textAlign : 'center',
    },*/
    ////////////////////////////////////////////////////////////
    list_container : {
        flex : 15,
        backgroundColor : APP.COLOR.BACKGROUD_COLOR,
        marginTop : 10,
    },
    list_style : {
        flex : 1,
    },
    loading_container : {
        position : 'absolute',
        left : 0,
        right : 0,
        top : 100,
        bottom : 0,
        alignItems : 'center',
        justifyContent : 'center',
    },
});


export default STYLE;
