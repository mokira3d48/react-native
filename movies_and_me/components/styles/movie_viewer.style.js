import APP          from './app.style';
import {StyleSheet} from 'react-native';


const STYLE = StyleSheet.create({
    container : {
        flex : 1,
        backgroundColor : APP.COLOR.BACKGROUD_COLOR,
        paddingBottom : 10,
    },
    detailContainer : {
        flex : 1,
        padding : 5,
        paddingBottom : 30,
    },
    image : {
        width : '100%',
        height : 250,
    },
    ///////////////////////////////////////////////////////////////
    favoriteStyle : {
        alignItems : 'center',
        marginTop : 20,
    },
    favoriteImage : {
        width : 60,
        height : 60,
    },
    ////////////////////////////////////////////////////////////
    originalTitle : {
        width : '100%',
        color : '#fff',
        fontSize : 26,
        fontWeight : 'bold',
        textAlign : 'center',
        marginTop : 10,
    },
    title : {
        width : '100%',
        color : APP.COLOR.TEXT_COLOR,
        fontSize : 20,
        fontWeight : 'bold',
        textAlign : 'center',
    },
    tagline : {
        width : '100%',
        color : APP.COLOR.ASSETS,
        fontSize : 17,
        //fontWeight : 'bold',
        textAlign : 'center',
        marginTop : 20,
        marginBottom : 30,
        paddingLeft : 20,
        paddingRight : 20,
    },
    description : {
        color : '#ccc',
        margin : 10,
        marginTop : 1,
        fontSize : 16,
        fontStyle : 'italic'
    },
    infoContainer : {
        margin : 10,
        marginTop : 20,
    },
    infoRow : {
        flex : 1,
        flexDirection : 'row',
        padding : 3,
    },
    attrName : {
        flex : 1,
        color : '#fff',
        padding : 1,
        fontSize : 16,
    },
    dataValue : {
        flex : 1,
        color : '#fff',
        padding : 1,
        fontSize : 16,
        fontWeight : 'bold',
        flexWrap : 'wrap',
    },
    shareTouchableFloating : {
        position : 'absolute',
        width : 50,
        height : 50,
        right : 20,
        bottom : 20,
        borderRadius : 30,
        backgroundColor : APP.COLOR.ASSETS + "dd",
        justifyContent : 'center',
        alignItems : 'center',
    },
    shareImage : {
        width : 55,
        height : 55,
        resizeMode : "contain",
    },
    shareTouchableHRight : {
        width : 60,
        height : 60,
    },

});


export default STYLE;
