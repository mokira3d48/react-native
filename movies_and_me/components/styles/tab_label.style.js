import APP from './app.style';
import {StyleSheet} from 'react-native';

const STYLE = StyleSheet.create({
    container : {
        backgroundColor : "transparent",
        padding : 5,
        width : "100%",
        height : "100%",
        alignItems : "center",
    },
    imageStyle : {
        width : 30,
        height : 30,
    },
});

export default STYLE;
