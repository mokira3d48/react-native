import APP from './app.style';
import {StyleSheet} from 'react-native';

const STYLE = StyleSheet.create({
	main_container : {
		height : 250,
		flexDirection : 'row',
		marginTop : -20,
		padding : 8,

	},
	image_container : {
		flex : 1,
		padding : 7,
		paddingTop : 14,
		paddingBottom : 14,
	},
	image : {
		flex : 1,
		backgroundColor : APP.COLOR.IMAGE_BACK_COLOR,
	},
	right_view : {
		flex : 2,
		padding : 8,

	},
	///////////////////////////////////////////////////////////
	title_view : {
		//flex : 3,
		flexDirection : 'row',
	},
	favorite : {
		//height : "100%",
		width : 30,
        height : 30,
        marginRight : 5,
	},
	title_text : {
		flex : 1,
		flexWrap : 'wrap',
		fontSize : 20,
		color : "#fff",
	},
	vote : {
		//flex : 1,
		fontWeight : 'bold',
		fontSize : 24,
		color : "#bbb",
	},
	////////////////////////////////////////////////////////
	description_view : {
		flex : 6,
		marginTop : 5,
	},
	description : {
		fontSize : 15,
		flex : 1,
		color : "#ddd",
		fontStyle : 'italic',
	},

	///////////////////////////////////////////////////////////
	date_view : {
		//flex : 1,
		marginTop : 5,
	},
	date : {
		textAlign : 'right',
		fontStyle : 'italic',
		color : '#999',
	}
});

export default STYLE;
