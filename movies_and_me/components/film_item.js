import React from 'react'
import { View, Text, Image, TouchableOpacity} from 'react-native'



/** Importation des styles. */
import APP      from './styles/app.style';
import STYLE    from './styles/film_item.style';

class FilmItem extends React.Component {
	constructor(props) {
		super(props);
		this.imguri = 'https://image.tmdb.org/t/p/w300';

	}

	render() {
		//console.log(this.props.film);
		return (
			<TouchableOpacity style={STYLE.main_container} onPress = {() => this.props.showDetails(this.props.film.id)}>
				<View style={STYLE.image_container} >
					<Image style={STYLE.image} source={{uri : this.imguri + this.props.film.poster_path}}></Image>
				</View>
				<View style={STYLE.right_view}>
					<View style={STYLE.title_view}>
						{this.props.isFavorite? <Image style = {STYLE.favorite} source = {require('../assets/ic_favorite.png')} /> : null}
						<Text style={STYLE.title_text}>{this.props.film.title}</Text>
						<Text style={STYLE.vote}>{this.props.film.vote_average}</Text>
					</View>
					<View style={STYLE.description_view}>
						<Text style={STYLE.description} numberOfLines={6}>{this.props.film.overview}</Text>
					</View>
					<View style={STYLE.date_view}>
						<Text style={STYLE.date}>Sorti le {this.props.film.release_date}</Text>
					</View>
				</View>
				
			</TouchableOpacity>
		)

	}
}


export default FilmItem;
