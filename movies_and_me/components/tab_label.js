import React from 'react';
import {Image}              from 'react-native';
import {TouchableOpacity}   from 'react-native';
import {useNavigation}      from '@react-navigation/native';

/** Importation des styles. */
import STYLE    from './styles/tab_label.style';


class TabLable extends React.Component {

    _navigate() {
        this.props.navigation.navigate(this.props.route);

    }


    render() {
        return (
            <TouchableOpacity 
                style={STYLE.container}
                onPress = {() => this._navigate()}
                >
                    <Image
                        style  = {STYLE.imageStyle}
                        source = {this.props.icon}
                        />
            </TouchableOpacity>
        );

    }
}

export default function(props) {
    const navigation = useNavigation();
    return <TabLable {...props} navigation = {navigation} />

}
