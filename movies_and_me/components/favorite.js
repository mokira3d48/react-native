import React from 'react'
import {StyleSheet, View, Text, Image, FlatList} from 'react-native'
import {connect} from 'react-redux';

import FilmItem  from './film_item';
import Loader    from './loader';

/** Importation des styles. */
import APP      from './styles/app.style';
import STYLE    from './styles/favorite.style';

const MAP_STATE_TO_PROPS = (state) => {
    return {
        favoritesMovie : state.favoritesMovie,
    };

};

class Favorite extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            movieData : [],
            isLoading : false,
        };

    }

    componentDidMount() {
        // on charge les films nouvellement sortis :
        this._loadFilm();

   }

    _makeid(length) {
        let result           = '';
        let characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        let charactersLength = characters.length;
        
        for ( var i = 0; i < length; i++ )
        result += characters.charAt(Math.floor(Math.random() * charactersLength));

        return result;

    }

    _loadFilm() {
        if (!this.state.isLoading)
            this.setState({ isLoading : true}, () => {
                this.setState({
                    movieData : this.props.favoritesMovie,
                    isLoading : false,
                });

            });

    }

    _showLoader() {
        if (this.state.isLoading)
            return (
                <Loader top='0' />
            );
        
        return null;

    }

    _showMovie = (movieid) => {
        //console.log(movie);
        this.props.navigation.navigate('Description du film', {movieid : movieid});

    }
    
    render() {
        return (
            <View style={STYLE.container}>
                <View style = { STYLE.list_container}>
                    <FlatList 
                        style        = {STYLE.list_style}
                        data         = {this.props.favoritesMovie} 
                        extraData    = {this.props.favoritesMovie}
                        keyExtractor = {(item)   => item.id.toString() + this._makeid(8)} 
                        renderItem   = {({item}) => <FilmItem 
                                                        film={item} 
                                                        isFavorite  = {true}
                                                        showDetails = {this._showMovie}
                                                    />}
                    />
                </View>

                {this._showLoader()}

            </View>
        );

    }
}



export default connect(MAP_STATE_TO_PROPS, null)(Favorite);
