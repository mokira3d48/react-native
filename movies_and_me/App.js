/** Importation globale. */
import React          from 'react';
import { StyleSheet } from 'react-native';
import { Provider }   from 'react-redux';
import {NavigationContainer}      from '@react-navigation/native';
import {createStackNavigator}     from '@react-navigation/stack';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';


/** Importation des composants. */
import Search       from './components/search';
import MovieViewer  from './components/movie_viewer';
import Favorite     from './components/favorite';
import TabLabel     from './components/tab_label';
import Profile      from './components/profile';


/** Importation des stores. */
import Store from './store/configure.store';

import APP from './components/styles/app.style';
//import STYLE from './components/styles/tab_label.style';

//import {StyleSheet} from 'react-native';

const MyTheme = {
  dark: false,
  colors: {
    primary: 'rgb(255, 45, 85)',
    background: APP.COLOR.BACKGROUD_COLOR,
    card: APP.COLOR.BACKGROUD_COLOR,
    text: 'rgb(255, 255, 255)',
    border: APP.COLOR.BACKGROUD_COLOR,
    notification: 'rgb(255, 69, 58)',
  },
};

const Stack = createStackNavigator();
const Tab   = createBottomTabNavigator();

function SearchFeature() {
  return (
    <Stack.Navigator>
      <Stack.Screen 
            name      = "Rechercher de film" 
            component = {Search} 
            //style     = {styles.container}
      />
      <Stack.Screen
            name      = "Description du film"
            component = {MovieViewer}
      />
    </Stack.Navigator>
  );
}

function FavoriteFeature() {
  return (
    <Stack.Navigator>
      <Stack.Screen 
            name      = "Rechercher de film" 
            component = {Favorite}
            //style     = {styles.container}
      />
      <Stack.Screen
            name      = "Description du film"
            component = {MovieViewer}
      />
    </Stack.Navigator>
  );
}

/*export default function App() {
  return (
    <Provider store = {Store}>
      <NavigationContainer>
        <Tab.Navigator>
          <Tab.Screen name = "Search" component = {SearchFeature} />
          <Tab.Screen name = "Favorite" component = {Favorite} />
          <Tab.Screen name = "Profile" component = {Profile} />
        </Tab.Navigator>
      </NavigationContainer>
    </Provider>
  );
}*/

export default function App() {
  return (
    <Provider store = {Store}>
      <NavigationContainer theme={MyTheme}>
        <Tab.Navigator initialRouteName = "Search" tabBarOptions = {{
            activeBackgroundColor : APP.COLOR.BACKGROUD_COLOR,
            activeTintColor : APP.COLOR.BACKGROUD_COLOR,
            inactiveBackgroundColor : APP.COLOR.MENU_BAR_COLOR,
            inactiveTintColor : APP.COLOR.MENU_BAR_COLOR,
        }}>
          <Tab.Screen name = "Search" component = {SearchFeature} options = {{
            tabBarLabel : () => (
              <TabLabel icon = {require('./assets/ic_search_tab.png')} route = "Search"></TabLabel>
            )
          }} />
          <Tab.Screen name = "Favorite" component = {FavoriteFeature} options = {{
            tabBarLabel : () => (
              <TabLabel icon = {require('./assets/ic_favorite_tab.png')} route = "Favorite"></TabLabel>
            )
          }} />
          <Tab.Screen name = "Profile" component = {Profile} options = {{
            tabBarLabel : () => (
              <TabLabel icon = {require('./assets/ic_profile_tab.png')} route = "Profile"></TabLabel>
            )
          }} />
        </Tab.Navigator>
      </NavigationContainer>
    </Provider>
  );
}


const styles = StyleSheet.create({
  container: {
    backgroundColor: '#000',
  },
});

